"""This module tests the fileoperations from utils.fileoperations"""
import unittest
import os
from pathlib import Path
from util.fileoperations import is_comment, get_filepaths_from_indexfile, \
    compress_into_single_file, load_paths_from_directory, \
    load_paths_from_indexfiles, load_paths


class TestIsComment(unittest.TestCase):
    """Tests is_comment function."""

    def test_is_comment(self):
        """Test whether commented line will yield True."""
        self.assertTrue(is_comment("#comment"))

    def test_is_comment_negative(self):
        """Test whether non-commented line will yield False."""
        self.assertFalse(is_comment("no comment"))


class TestFileoperations(unittest.TestCase):
    """Base Class for all later test cases. Provides structure for tests involving files.

    Attributes:
        temp_dir: Path to folder that will house all temporary files.
            Will be deleted during tear down.
    """

    def setUp(self):
        """Set up test. Will be called before each test.

        Will call _setup_temp_files which subclasses should override.
        """
        self.temp_dir = Path(r"tmp")
        self._make_dir(self.temp_dir)
        self._setup_temp_files()

    def _setup_temp_files(self):
        """Abstract method for file setup."""

    def _delete_temp_files(self):
        """Abstract method for file tear down."""

    @staticmethod
    def _make_file(path: Path, filename: Path, content: str):
        """Creates or overwrites the file at path/filename with content.

        Args:
            path: Path to directory where to create file.
            filename: Filename to be used for new file.
            content: Content of the new file.
        """
        file = open(path/filename, "w")
        file.write(content)
        file.close()

    @staticmethod
    def _make_dir(path: Path):
        """Creates a directory if not present.

        Args:
            path: Path where to create directory.
        """
        if not os.path.isdir(path):
            os.mkdir(path)

    def tearDown(self):
        """Tear down test. Will be called after each test."""
        self._delete_temp_files()
        self._delete_dir(self.temp_dir)

    @staticmethod
    def _delete_file(path: Path, filename: Path):
        """Delete file at path/filename.

        Args:
            path: Path to folder conataining filename.
            filename: Name of file to delete.
        """
        if os.path.exists(path/filename):
            os.remove(path/filename)

    @staticmethod
    def _delete_dir(path: Path):
        """Delete directory at path.

        Args:
            path: Path to directory to delete.
        """
        if os.path.isdir(path):
            os.rmdir(path)

    @staticmethod
    def _read_file(path: Path, filename: Path) -> str:
        """Get file's content.

        Args:
            path: Path to directory containing filename.
            filename: Filename of file to be read.

        Returns:
            Content of the file.
        """
        with open(path/filename, encoding="utf8") as file:
            return file.read()


class TestGetFilepathsFromIndexfile(TestFileoperations):
    """Tests get_filepaths_from_indexfile function."""

    _filename_index = r"index.txt"
    _filecontent_index = """abc/cde/fg.txt \nhij/klm/nop.txt"""

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        self._make_file(self.temp_dir, self._filename_index, self._filecontent_index)

    def test_get_filepaths_from_indexfile(self):
        """Test get_filepaths_from_indexfile's returned paths"""
        self.assertEqual(
            get_filepaths_from_indexfile(self.temp_dir/self._filename_index),
            [str(Path('abc/cde/fg.txt')), str(Path('hij/klm/nop.txt'))]
        )

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_index)


class TestCompressIntoSingleFile(TestFileoperations):
    """Tests compress_into_single_file function."""

    _filename_a = Path(r"A.txt")
    _filename_b = Path(r"B.txt")
    _filename_ab = Path("AB.txt")
    _filecontent_a = """A"""
    _filecontent_b = """B"""

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        self._make_file(self.temp_dir, self._filename_a, self._filecontent_a)
        self._make_file(self.temp_dir, self._filename_b, self._filecontent_b)

    def test_compress_into_single_file(self):
        """Test compress_into_single_file's created file"""
        filepaths = [self.temp_dir/self._filename_a, self.temp_dir/self._filename_b]
        compress_into_single_file(filepaths, self.temp_dir/self._filename_ab)
        self.assertEqual(
            self._read_file(self.temp_dir, self._filename_ab),
            "AB"
            )

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_a)
        self._delete_file(self.temp_dir, self._filename_b)
        self._delete_file(self.temp_dir, self._filename_ab)


class TestLoadPathsFromDirectory(TestFileoperations):
    """Tests load_paths_from_directory function."""

    _filename_a = Path(r"A.txt")
    _filename_b = Path(r"B.txt")
    _filename_c = Path(r"C.txt")
    _dirname = Path("C")
    _filecontent_a = """A"""
    _filecontent_b = """B"""
    _filecontent_c = """C"""

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        self._make_file(self.temp_dir, self._filename_a, self._filecontent_a)
        self._make_file(self.temp_dir, self._filename_b, self._filecontent_b)
        self._make_dir(self.temp_dir/self._dirname)
        self._make_file(self.temp_dir/self._dirname, self._filename_c, self._filecontent_b)

    def test_load_paths_from_directory(self):
        """Test load_paths_from_directory's returned paths"""
        self.assertEqual(
            load_paths_from_directory(self.temp_dir),
            [str(Path('tmp/A.txt')), str(Path('tmp/B.txt')), str(Path('tmp/C/C.txt'))]
        )

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_a)
        self._delete_file(self.temp_dir, self._filename_b)
        self._delete_file(self.temp_dir/self._dirname, self._filename_c)
        self._delete_dir(self.temp_dir/self._dirname)


class TestLoadPathsFromIndexfiles(TestFileoperations):
    """Tests load_paths_from_indexfiles function."""

    _filename_a = Path(r"A.txt")
    _filename_b = Path(r"B.txt")
    _filename_c = Path(r"C.txt")
    _dirname = Path("C")
    _filename_index_a = Path(r"index_A.txt")
    _filename_index_b = Path(r"index_B.txt")
    _filecontent_index_a = str(_filename_a)
    _filecontent_index_b = str(_filename_b) + '\n' + str(_dirname) + '/' + str(_filename_c)

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        self._make_file(self.temp_dir, self._filename_index_a, self._filecontent_index_a)
        self._make_file(self.temp_dir, self._filename_index_b, self._filecontent_index_b)

    def test_load_paths_from_indexfiles(self):
        """Test load_paths_from_indexfiles's returned paths"""
        paths_to_index_files = [
                self.temp_dir/self._filename_index_a,
                self.temp_dir/self._filename_index_b
            ]
        self.assertEqual(
            load_paths_from_indexfiles(self.temp_dir, paths_to_index_files),
            [str(Path('tmp/A.txt')), str(Path('tmp/B.txt')), str(Path('tmp/C/C.txt'))]
        )

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_index_a)
        self._delete_file(self.temp_dir, self._filename_index_b)


class TestLoadPathsWithoutIndexfiles(TestFileoperations):
    """Tests load_paths function when loading from directory."""

    _filename_a = Path(r"A.txt")
    _filename_b = Path(r"B.txt")
    _filename_c = Path(r"C.txt")
    _dirname = Path("C")
    _filecontent_a = """A"""
    _filecontent_b = """B"""
    _filecontent_c = """C"""

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        self._make_file(self.temp_dir, self._filename_a, self._filecontent_a)
        self._make_file(self.temp_dir, self._filename_b, self._filecontent_b)
        self._make_dir(self.temp_dir/self._dirname)
        self._make_file(self.temp_dir/self._dirname, self._filename_c, self._filecontent_b)

    def test_load_paths(self):
        """Test load_paths's returned paths"""
        self.assertEqual(
            load_paths(self.temp_dir),
            [str(Path('tmp/A.txt')), str(Path('tmp/B.txt')), str(Path('tmp/C/C.txt'))]
        )

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_a)
        self._delete_file(self.temp_dir, self._filename_b)
        self._delete_file(self.temp_dir/self._dirname, self._filename_c)
        self._delete_dir(self.temp_dir/self._dirname)


class TestLoadPathsWithIndexfiles(TestFileoperations):
    """Tests load_paths function when loading from indexfiles."""

    _filename_a = Path(r"A.txt")
    _filename_b = Path(r"B.txt")
    _filename_c = Path(r"C.txt")
    _dirname = Path("C")
    _filename_index_a = Path(r"index_A.txt")
    _filename_index_b = Path(r"index_B.txt")
    _filecontent_index_a = str(_filename_a)
    _filecontent_index_b = str(_filename_b) + '\n' + str(_dirname) + '/' + str(_filename_c)

    def _setup_temp_files(self):
        """Sets up temporary files for tests."""
        self._make_file(self.temp_dir, self._filename_index_a, self._filecontent_index_a)
        self._make_file(self.temp_dir, self._filename_index_b, self._filecontent_index_b)

    def test_load_paths(self):
        """Test load_paths's returned paths"""
        paths_to_index_files = [
                self.temp_dir/self._filename_index_a,
                self.temp_dir/self._filename_index_b
            ]
        self.assertEqual(
            load_paths(self.temp_dir, paths_to_index_files),
            [str(Path('tmp/A.txt')), str(Path('tmp/B.txt')), str(Path('tmp/C/C.txt'))]
        )

    def _delete_temp_files(self):
        """Deletes temporary files that were created for testing."""
        self._delete_file(self.temp_dir, self._filename_index_a)
        self._delete_file(self.temp_dir, self._filename_index_b)
