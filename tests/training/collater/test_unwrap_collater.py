import unittest
import torch
from training.collater.unwrap_collater import UnwrapCollater


class TestUnwrapCollater(unittest.TestCase):
    """Tests Collater class."""

    def setUp(self):
        """Set up test. Will be called before each test."""
        self._collater = UnwrapCollater()

    def test_collate(self):
        """Test Collater batching."""
        batch = self._collater(self._sample_data)
        self.assertTrue(
            torch.all(torch.eq(
                batch["input_ids"],
                torch.Tensor([
                    [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.],
                    [0., 0., 0., 0., 0., 0., 0., 1., 2.,  3.],
                    [0., 0., 0., 0., 0., 1., 2., 3., 4.,  5.]
                ])
            ))
        )
        self.assertTrue(
            torch.all(torch.eq(
                batch["labels"],
                torch.Tensor([
                    [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.],
                    [0., 0., 0., 0., 0., 0., 0., 1., 2.,  3.],
                    [0., 0., 0., 0., 0., 1., 2., 3., 4.,  5.]
                ])
            ))
        )
        self.assertTrue(len(batch) == 2)

    _sample_data = [torch.Tensor([
            [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.],
            [0., 0., 0., 0., 0., 0., 0., 1., 2.,  3.],
            [0., 0., 0., 0., 0., 1., 2., 3., 4.,  5.]
        ])]
