import unittest
import torch
from training.collater.collater import Collater
from tokenizer.tokenizer import WordPieceTokenizer


class TestCollater(unittest.TestCase):
    """Tests Collater class."""

    def setUp(self):
        """Set up test. Will be called before each test."""
        self._tokenizer = WordPieceTokenizer(vocab_file=r"tokenizer/PythonP150-vocab.txt")
        self._collater = Collater(self._tokenizer)

    def test_collate(self):
        """Test Collater batching."""
        batch = self._collater(self._sample_data)
        self.assertTrue(
            torch.all(torch.eq(
                batch["input_ids"],
                torch.Tensor([
                    [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.],
                    [0., 0., 0., 0., 0., 0., 0., 1., 2.,  3.],
                    [0., 0., 0., 0., 0., 1., 2., 3., 4.,  5.]
                ])
            ))
        )
        self.assertTrue(
            torch.all(torch.eq(
                batch["labels"],
                torch.Tensor([
                    [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.],
                    [0., 0., 0., 0., 0., 0., 0., 1., 2.,  3.],
                    [0., 0., 0., 0., 0., 1., 2., 3., 4.,  5.]
                ])
            ))
        )
        self.assertTrue(len(batch) == 2)

    _sample_data = [
                    torch.Tensor([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
                    torch.Tensor([1, 2, 3]),
                    torch.Tensor([1, 2, 3, 4, 5])
                ]
