import unittest
import os
import torch
from transformers import TransfoXLLMHeadModel
from tokenizer.tokenizer import WordPieceTokenizer
from generation.generator.generator import Generator


class TestGenerator(unittest.TestCase):
    """Tests the ouput generated using the Generator class.

    These tests require a model and will be skipped if no model is present.

    The output can't be tested directly beyond ensuring that generation happens
    wihtout throwing an exception. All tests will print their output to allow
    semantical evaluation.
    """

    def setUp(self):
        """Set up test. Will be called before each test."""
        self._generator = Generator(
                r"tokenizer/PythonP150-vocab.txt",
                r"model",
                verbose=True,
                max_num_of_generated_tokens=30
            )
        self._tokenizer = WordPieceTokenizer(
            vocab_file=r"tokenizer/PythonP150-vocab.txt",
            )

        self._model = TransfoXLLMHeadModel.from_pretrained(r"model")
        self._model.eval()

    @unittest.skipIf(not os.path.isdir('model') or not os.listdir('model'),
                     "No Model present. Skipped model depend test.")
    def test_generator(self):
        """Test generation using Generator class."""
        print("Input: ", self._sample_text)
        print("Output: ")

        rank = 1
        for predicted_seq in self._generator(self._sample_text.replace('\n', '[EOL]')):
            print("{}. ".format(rank) + predicted_seq)
            rank += 1

    @unittest.skipIf(not os.path.isdir('model') or not os.listdir('model'),
                     "No Model present. Skipped model depend test.")
    def test_generate_predicted_sequences_max_length(self):
        """Test the max number of tokens per prediction."""
        encoding = self._tokenizer.encode(self._sample_text)
        input_ids = torch.tensor([encoding.ids])
        mems = self._model(input_ids=input_ids)[1]

        # Intentionally testing internal method
        # pylint: disable=protected-access
        for predicted_sequence in self._generator._generate_predicted_sequences(
                        mems,
                        torch.tensor([10, 29, 1055])
                    ):
            self.assertTrue(len(predicted_sequence) <= 30)

    _sample_text = """       # compute attention probability
        if attn_mask is not None and torch.sum(attn_mask).item():
            attn_mask = attn_mask == 1  # Switch to bool
            if attn_mask.dim() == 2:
                if next(self.parameters()).dtype == torch.float16:
                    attn_score = (
                        attn_score.float().masked_fill(attn_mask[None, :, :, None], -65000).type_as(attn_score)
                    )
                else:
                    attn_score = attn_score.float().masked_fill(attn_mask[None, :, :, None], -1e30).type_as(attn_score)
            elif attn_mask.dim() == 3:
                if next(self.parameters()).dtype == torch.float16:
                    attn_score = attn_score.float().masked_fill(attn_mask[:, :, :, None], -65000).type_as(attn_score)
                else:
                    attn_score = attn_score.float().masked_fill(attn_mask[:, :, :, None], -1e30).type_as(attn_score)

        # [qlen x klen x bsz x n_head]
        attn_prob = F.softmax(attn_score, dim=1)
        attn_prob = self.dropatt(attn_prob)

        # Mask heads if we want to
        if head_mask is not None:
            attn_prob = attn_prob * head_mask

        # compute attention vector
        attn_vec = torch.einsum("ijbn,jbnd->ibnd", (attn_prob, w_head_v))

        # [qlen x bsz x n_head x d_head]
        attn_vec = attn_vec.contiguous().view(attn_vec.size(0), attn_vec.size(1), self.n_head * self.d_head)

        # linear projection
        attn_out = self.o_net(attn_vec)
        attn_out = self.drop(attn_out)

        if self.pre_lnorm:
            # residual connection
            outputs = [w + attn_out]
        else:
            # residual connection + layer normalization
            outputs = [self.layer_norm"""
