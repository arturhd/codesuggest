"""
Script to train tokenizer. Training is based on a frequency analysis
of the given corpus.
"""
import sys
from typing import List
from tokenizer.tokenizer import WordPieceTokenizer
from util.fileoperations import load_paths


def train_tokenizer(path_to_dataset: str, paths_to_index_files: List[str]):
    """Initiate training on the given corpus

    Args:
        path_to_dataset: Path to the directory containing the dataset.
        paths_to_index_files: List of files that contains one filepath per line.
            Contained filepaths must be relative to path_to_dataset.

    """
    paths_to_datafiles = load_paths(path_to_dataset, paths_to_index_files)
    tokenizer = WordPieceTokenizer()

    tokenizer.train(
        files=paths_to_datafiles,
        vocab_size=52000,
        )

    tokenizer.save_model('tokenizer/training', 'PythonP150')


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: train_tokenizer path_to_dataset [path_to_index, ... , path_to_index]")
    else:
        train_tokenizer(sys.argv[1], sys.argv[2:])
