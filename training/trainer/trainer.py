import warnings
from typing import Any, Dict, List, Optional, Tuple, Union
import transformers
import torch
import numpy as np
from torch import nn
from torch.utils.data.dataloader import DataLoader
from torch.utils.data.dataset import Dataset
from torch.utils.data.distributed import DistributedSampler
from torch.utils.data.sampler import RandomSampler, SequentialSampler
from transformers.trainer_utils import nested_concat, nested_numpify
from transformers.trainer_utils import PredictionOutput, EvalPrediction
from transformers.trainer import get_tpu_sampler, logger, is_torch_tpu_available
from transformers.trainer import SequentialDistributedSampler
from transformers.trainer import distributed_concat, nested_xla_mesh_reduce
from transformers.trainer import distributed_broadcast_scalars
from tqdm.auto import tqdm


# The script has conditional imports for xm, amp, and pl but pylint isn't smart
# enough to recognize them.
# pylint: disable=undefined-variable
# Pylint detects that self._past is not declared in __init__ this is also the
# case for the unchanged huggingface trainer and will therefore not be changed.
# pylint: disable=attribute-defined-outside-init
# Pylint detects overcomplexity. This is a problem with the original
# huggingface code and won't be addressed here.
# pylint: disable=too-many-statements
# pylint: disable=too-many-branches
# pylint: disable=too-many-locals
class Trainer(transformers.Trainer):
    """Adapted form of huggingfaces Trainer"""

    def get_train_dataloader(self) -> DataLoader:
        """Load Dataloader for training

        Implements pull request https://github.com/huggingface/transformers/pull/5834
        Necessary to support IterableDataset

        Returns:
            Dataloader for training.

        """
        if isinstance(self.train_dataset, torch.utils.data.IterableDataset):
            train_sampler = None
        elif self.train_dataset is None:
            raise ValueError("Trainer: training requires a train_dataset.")
        elif is_torch_tpu_available():
            train_sampler = get_tpu_sampler(self.train_dataset)
        else:
            train_sampler = (
                RandomSampler(self.train_dataset)
                if self.args.local_rank == -1
                else DistributedSampler(self.train_dataset)
            )
        data_loader = DataLoader(
            self.train_dataset,
            batch_size=self.args.train_batch_size,
            sampler=train_sampler,
            collate_fn=self.data_collator,
            drop_last=self.args.dataloader_drop_last,
        )

        return data_loader

    def get_eval_dataloader(self, eval_dataset: Optional[Dataset] = None) -> DataLoader:
        """Load Dataloader for evaluation

        Implements pull request https://github.com/huggingface/transformers/pull/5834
        Necessary to support IterableDataset

        Args:
            eval_dataset: If provided, will override self.eval_dataset.

        Returns:
            Dataloader for evaluation.

        """
        if eval_dataset is None and self.eval_dataset is None:
            raise ValueError("Trainer: evaluation requires an eval_dataset.")

        eval_dataset = eval_dataset if eval_dataset is not None else self.eval_dataset

        if isinstance(eval_dataset, torch.utils.data.IterableDataset):
            sampler = None
        elif is_torch_tpu_available():
            sampler = SequentialDistributedSampler(
                eval_dataset, num_replicas=xm.xrt_world_size(), rank=xm.get_ordinal()
            )
        elif self.args.local_rank != -1:
            sampler = SequentialDistributedSampler(eval_dataset)
        else:
            sampler = SequentialSampler(eval_dataset)

        data_loader = DataLoader(
            eval_dataset,
            sampler=sampler,
            batch_size=self.args.eval_batch_size,
            collate_fn=self.data_collator,
            drop_last=self.args.dataloader_drop_last,
        )

        return data_loader

    # This method refers to members that are not yet present in the released version of
    # huggingface. This override will be kept to ensure forward compatibility.
    # pylint: disable=no-member
    def get_test_dataloader(self, test_dataset: Dataset) -> DataLoader:
        """Load Dataloader for testing

        Implements pull request https://github.com/huggingface/transformers/pull/5834
        Necessary to support IterableDataset

        Args:
            test_dataset: The test dataset to use.

        Returns:
            Dataloader for testing.

        """
        # We use the same batch_size as for eval.
        if isinstance(self.test_dataset, torch.utils.data.IterableDataset):
            sampler = None
        elif is_torch_tpu_available():
            sampler = SequentialDistributedSampler(
                test_dataset, num_replicas=xm.xrt_world_size(), rank=xm.get_ordinal()
            )
        elif self.args.local_rank != -1:
            sampler = SequentialDistributedSampler(test_dataset)
        else:
            sampler = SequentialSampler(test_dataset)

        data_loader = DataLoader(
            test_dataset,
            sampler=sampler,
            batch_size=self.args.eval_batch_size,
            collate_fn=self.data_collator,
            drop_last=self.args.dataloader_drop_last,
        )
        return data_loader
    # pylint: enable=no-member

    def _training_step(
                self,
                model: nn.Module,
                inputs: Dict[str, Union[torch.Tensor, Any]],
                optimizer: torch.optim.Optimizer
            ) -> float:
        """Perform a single training step

        Override to deal with TransfoXL's special loss.
        TransfoXL is the only model that doesn't return loss as a scalar.

        Args:
            model: The model that is being trained.
            inputs: The inputs to feed to model for this step.
            optimizer: The optimizer used during training.

        Returns:
            Training loss.

        """
        model.train()
        for k, v in inputs.items():
            if isinstance(v, torch.Tensor):
                inputs[k] = v.to(self.args.device)

        if self.args.past_index >= 0 and self._past is not None:
            inputs["mems"] = self._past

        outputs = model(**inputs)

        # Output[0] should return loss in all huggingface models.
        # However, Transfo-XL returns the Cross Entropy Loss for each token as output[0].
        # Transfo XL recommends loss only on the second half of the sequences to
        # increase generalisation. (seq_length = outputs[0].size(1) [:,seq_length//2:])
        loss = outputs[0].mean()

        if self.args.past_index >= 0:
            self._past = outputs[self.args.past_index]

        if self.args.n_gpu > 1:
            loss = loss.mean()  # mean() to average on multi-gpu parallel training
        if self.args.gradient_accumulation_steps > 1:
            loss = loss / self.args.gradient_accumulation_steps

        if self.args.fp16:
            with amp.scale_loss(loss, optimizer) as scaled_loss:
                scaled_loss.backward()
        else:
            loss.backward()

        return loss.item()

    def prediction_step(
                self,
                model: nn.Module,
                inputs: Dict[str, Union[torch.Tensor, Any]],
                prediction_loss_only: bool
            ) -> Tuple[Optional[float], Optional[torch.Tensor], Optional[torch.Tensor]]:
        """Perform an evaluation step on model using inputs

        Override to deal with TransfoXL's inconsistent beviour depending on whether
        or not labels are passed as input.

        Args:
            model: The model to evaluate.
            inputs: The inputs and targets of the model.
                The dictionary will be unpacked before being fed to the model.
                Most models expect the targets under the argument "labels".
            prediction_loss_only: Whether or not to return the loss only.

        Return:
            A tuple with the loss, logits and labels (each being optional).

        """
        has_labels = all(inputs.get(k) is not None for k in self.args.label_names)
        inputs = self._prepare_inputs(inputs)

        # Transformer-XL once again defies the interfaces of the remaining
        # huggingface-library. While undocumented, the code reveals that
        # transfo-xl returns loss only when labels are passed
        # (i.e. doesn't return predictions)
        # For eval there are thus only two options:
        # A. rewrite TransfoXL to return predictions and loss
        # B. run the model twice. Once with, once without labels
        #
        # A. requires a massive rewrite of the model class
        # B. effectively halves eval performance
        # A. is unfeasable at this point and thus B. was chosen.
        with torch.no_grad():
            outputs = model(**inputs)
            if has_labels:
                # The .mean() is to reduce in case of distributed training
                loss = outputs[0].mean().item()

                # Rerun to get predictions
                input_without_labels = dict(inputs)
                for label_keys in self.args.label_names:
                    if label_keys in input_without_labels:
                        del input_without_labels[label_keys]
                output_without_loss = model(**input_without_labels)

                # loss is not returned if no labels are passed
                # later outputs are no tensors and break subsequent parts
                # of the script
                logits = (output_without_loss[0],)
            else:
                loss = None
                # Slicing so we get a tuple even if `outputs` is a `ModelOutput`.
                logits = outputs[:]
            if self.args.past_index >= 0:
                self._past = outputs[
                        self.args.past_index if has_labels else self.args.past_index - 1
                    ]

        if prediction_loss_only:
            return (loss, None, None)

        logits = tuple(logit.detach() for logit in logits)
        if len(logits) == 1:
            logits = logits[0]

        if has_labels:
            labels = tuple(inputs.get(name).detach() for name in self.args.label_names)
            if len(labels) == 1:
                labels = labels[0]
        else:
            labels = None

        return (loss, logits, labels)

    def prediction_loop(
        self, dataloader: DataLoader, description: str, prediction_loss_only: Optional[bool] = None
    ) -> PredictionOutput:
        """Prediction/evaluation loop, shared by Trainer.evaluate() and Trainer.predict()

        Works both with or without labels.

        Override to change signature of metric functions. Metric functions now
        get loss and EvalPrediction.

        Args:
            dataloader: The data used for the predictions.
            description: Description of prediction to display in console.
            prediction_loss_only: Whether or not to return the loss only.

        Return:
            The final metrics as PredictionOutput.

        """
        # Pylint doesn't understand introspection and complains about use of _prediction_loop.
        # This is a false positive.
        # pylint: disable=no-member
        if hasattr(self, "_prediction_loop"):
            warnings.warn(
                "The `_prediction_loop` method is deprecated and won't be called \
                    in a future version, define `prediction_loop` in your subclass.",
                FutureWarning,
            )
            return self._prediction_loop(
                    dataloader,
                    description,
                    prediction_loss_only=prediction_loss_only
                )
        # pylint: enable=no-member

        prediction_loss_only = (
                prediction_loss_only if prediction_loss_only is not None
                else self.args.prediction_loss_only
            )

        assert not getattr(
            self.model.config, "output_attentions", False
        ), "The prediction loop does not work with `output_attentions=True`."
        assert not getattr(
            self.model.config, "output_hidden_states", False
        ), "The prediction loop does not work with `output_hidden_states=True`."

        model = self.model
        # multi-gpu eval
        if self.args.n_gpu > 1:
            model = torch.nn.DataParallel(model)
        else:
            model = self.model
        # Note: in torch.distributed mode, there's no point in wrapping the model
        # inside a DistributedDataParallel as we'll be under `no_grad` anyways.

        batch_size = dataloader.batch_size
        logger.info("***** Running %s *****", description)
        logger.info("  Num examples = %d", self.num_examples(dataloader))
        logger.info("  Batch size = %d", batch_size)
        eval_losses: List[float] = []
        preds: torch.Tensor = None
        label_ids: torch.Tensor = None
        model.eval()

        if is_torch_tpu_available():
            dataloader = pl.ParallelLoader(
                    dataloader,
                    [self.args.device]
                ).per_device_loader(self.args.device)

        if self.args.past_index >= 0:
            self._past = None

        disable_tqdm = not self.is_local_process_zero() or self.args.disable_tqdm
        for inputs in tqdm(dataloader, desc=description, disable=disable_tqdm):
            loss, logits, labels = self.prediction_step(model, inputs, prediction_loss_only)
            batch_size = inputs[list(inputs.keys())[0]].shape[0]
            if loss is not None:
                eval_losses.extend([loss] * batch_size)
            if logits is not None:
                preds = logits if preds is None else nested_concat(preds, logits, dim=0)
            if labels is not None:
                label_ids = labels if label_ids is None \
                    else nested_concat(label_ids, labels, dim=0)

        if self.args.past_index and hasattr(self, "_past"):
            # Clean the state at the end of the evaluation loop
            delattr(self, "_past")

        if self.args.local_rank != -1:
            # In distributed mode, concatenate all results from all nodes:
            if preds is not None:
                preds = distributed_concat(
                        preds,
                        num_total_examples=self.num_examples(dataloader)
                    )
            if label_ids is not None:
                label_ids = distributed_concat(
                        label_ids,
                        num_total_examples=self.num_examples(dataloader)
                    )
        elif is_torch_tpu_available():
            # tpu-comment: Get all predictions and labels from all worker shards of eval dataset
            if preds is not None:
                preds = nested_xla_mesh_reduce(
                        preds,
                        "eval_preds"
                    )
            if label_ids is not None:
                label_ids = nested_xla_mesh_reduce(
                        label_ids,
                        "eval_label_ids"
                    )
            if eval_losses is not None:
                eval_losses = xm.mesh_reduce(
                        "eval_losses",
                        torch.tensor(eval_losses),
                        torch.cat
                    ).tolist()

        # Finally, turn the aggregated tensors into numpy arrays.
        if preds is not None:
            preds = nested_numpify(preds)
        if label_ids is not None:
            label_ids = nested_numpify(label_ids)

        metrics = {}
        if len(eval_losses) > 0:
            if self.args.local_rank != -1:
                metrics["eval_loss"] = (
                    distributed_broadcast_scalars(
                        eval_losses,
                        num_total_examples=self.num_examples(dataloader)
                    )
                    .mean()
                    .item()
                )
            else:
                metrics["eval_loss"] = np.mean(eval_losses)

        if self.compute_metrics is not None and preds is not None and label_ids is not None:
            metrics.update(
                self.compute_metrics(
                        metrics["eval_loss"],
                        EvalPrediction(predictions=preds, label_ids=label_ids)
                    )
                )

        # Prefix all keys with eval_
        for key in list(metrics.keys()):
            if not key.startswith("eval_"):
                metrics[f"eval_{key}"] = metrics.pop(key)

        return PredictionOutput(predictions=preds, label_ids=label_ids, metrics=metrics)
