"""Script to evaluate a TransfoXLLMHeadModel model."""
import argparse
from transformers import TrainingArguments
from transformers import TransfoXLLMHeadModel
from training.trainer.trainer import Trainer
from training.collater.unwrap_collater import UnwrapCollater
from training.dataset.batched_iterable_dataset import BatchedIterableDataset
from training.evaluation.metrics.mrr import mrr
from training.evaluation.metrics.perplexity import perplexity
from training.evaluation.metrics.combine_metrics import make_combined_metric


def main(args):
    """Evaluates model on data and calculates loss, perplexity and mrr score.

    Args:
        args: Parsed commandline arguments.

    """
    model = TransfoXLLMHeadModel.from_pretrained(args.filepath_to_model)

    eval_dataset = BatchedIterableDataset(
        filepath_to_tensorized_index=args.filepath_to_tensorized_index,
        filepath_to_tensorized_data=args.filepath_to_tensorized_data,
        batch_size=args.batch_size,
        )

    eval_args = TrainingArguments(
        output_dir=args.filepath_to_model,
        do_eval=True,
        logging_steps=args.logging_steps,
        past_index=2 if args.use_past else -1,
        per_device_train_batch_size=1  # Batching is done inside the dataset
        )

    collater = UnwrapCollater()

    metrics = make_combined_metric([mrr, perplexity])

    trainer = Trainer(
        model=model,
        args=eval_args,
        eval_dataset=eval_dataset,
        data_collator=collater,
        compute_metrics=metrics
        )

    metrics = trainer.evaluate()


def parse_arguments():
    """Parses the commandline arguments.

    Returns:
        Arguments extracted from the commandline.

    """
    parser = argparse.ArgumentParser(description='eval model')
    parser.add_argument('--filepath_to_tensorized_index', type=str,
                        default='../py150/tiny_txt_tensorized_index.pt',
                        help='path to index file for tensorized data')
    parser.add_argument('--filepath_to_tensorized_data', type=str,
                        default='../py150/tiny_txt_tensorized_data.pt',
                        help='path to tensorized data file')
    parser.add_argument('--filepath_to_model', type=str,
                        default='model',
                        help='path to directory of model')
    parser.add_argument('--logging_steps', type=int,
                        default=250,
                        help='logging intervall for tensorboard')
    parser.add_argument('--batch_size', type=int,
                        default=6,
                        help='batch size')
    parser.add_argument('--use_past', default=False, action='store_true',
                        help='evaluation using the past')
    args = parser.parse_args()

    return args


if __name__ == "__main__":
    main(parse_arguments())
