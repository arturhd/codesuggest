"""Contains function to combine multiple metric functions into one"""
from typing import Callable, Dict, List
from transformers.trainer_utils import EvalPrediction


def make_combined_metric(
            metrics: List[Callable[[float, EvalPrediction], Dict]]
        ) -> Callable[[float, EvalPrediction], Dict]:
    """Combine metric functions into one function

    Args:
        metrics: List of metric functions (cf. huggingface trainer eval).

    Returns:
        New metric function that combines all input functions.

    """
    return lambda loss, predictions: _combine_output(metrics, loss, predictions)


def _combine_output(
            metrics: List[Callable[[float, EvalPrediction], Dict]],
            loss: float,
            predictions: EvalPrediction
        ) -> Dict:
    """Pass input to metric functions and combine output

    Args:
        metrics: List of metric functions (cf. huggingface trainer eval).
        loss: Evaluation loss.
        predictions: Combined model output from entire evaluation dataset.

    Returns:
        Combined output from all metric functions.

    """
    combined_output = {}
    for metric in metrics:
        combined_output.update(metric(loss, predictions))
    return combined_output
