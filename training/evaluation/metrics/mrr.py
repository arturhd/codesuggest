"""Mean Reciprocal Rank as metric function for evaluation"""
from typing import Dict
from transformers.trainer_utils import EvalPrediction
import numpy as np


# loss is necessary to conform to interface
# pylint: disable=unused-argument
def mrr(loss: float, eval_predictions: EvalPrediction) -> Dict:
    """Calculate Mean Reciprocal Rank from EvalPrediction

    To speed up calculations only the 100 highest ranking tokens are considered.

    Args:
        loss: Evaluation loss.
        predictions: Combined model output from entire evaluation dataset.

    Returns:
        Dict containing mrr score.

    """
    predictions = eval_predictions.predictions
    top_100_predictions = get_top_k_tokens(predictions, 100)
    labels = eval_predictions.label_ids
    reciprocal_rank = 0.0
    count = 0
    for sample in range(labels.shape[0]):
        for token in range(labels.shape[1]-1):
            reciprocal_rank += get_reciprocal_rank(
                # Need to shift to match predictions to labels
                labels[sample, token + 1],
                top_100_predictions[sample, token]
                )
            count += 1
            if count % 1000 == 0:
                print("Calculating MRR: {0}".format(count/labels.size))
    return {"mrr": reciprocal_rank/labels.size}


def get_top_k_tokens(predictions: np.array, k: int) -> np.array:
    """Calculate indeces of k biggest elements in last array dimension

    Args:
        predictions: Array with last dimension >= k.
        k: How many elements to rank.

    Returns:
        Array of identical dimensions, except last dimension is now k.
        Contains indeces of top k elements in input array in order desc.

    """
    top_k = np.zeros(
                shape=(
                        predictions.shape[0],
                        predictions.shape[1],
                        k
                    )
                )
    for i in range(predictions.shape[0]):
        for j in range(predictions.shape[1]):
            tokens = np.argpartition(predictions[i, j], -k)[-k:]
            top_k[i, j] = tokens[np.argsort(predictions[i, j][tokens])[::-1]]
    return top_k


def get_reciprocal_rank(token: int, top_tokens: np.array) -> float:
    """Calculate reciprocal rank of token in top_tokens

    Args:
        token: Token of which to get rank.
        top_tokens: One-dimensional array of tokens. The rank of a token is its
            index in the array.

    Returns:
        Reciprocal rank of the input token in the given token array.

    """
    for rank in range(top_tokens.shape[0]):
        if top_tokens[rank] == token:
            return 1.0/(rank + 1)
    return 0
