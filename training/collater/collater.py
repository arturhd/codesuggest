from typing import Dict, List
import torch
import torch.nn.functional as F
from tokenizers import Tokenizer


class Collater:
    """Collates lists of tensors of heterogeneous length

    Attributes:
        tokenizer: Tokenizer used for padding.
    """

    def __init__(self, tokenizer: Tokenizer):
        """Initializes collater from Tokenizer

        Args:
            tokenizer: This tokenizer defines the padding token used to pad
                shorter tensors.

        """
        self.tokenizer = tokenizer

    def __call__(self, examples: List[torch.Tensor]) -> Dict[str, torch.Tensor]:
        """Collate a list of tensors

        Args:
            examples: List of samples. Tensors must be identical in all but the
                first dimension.

        Returns:
            Batch made of the input tensors. All tensors are padded to the same
            length and then stacked along new dimension.

            Returns same data as "input_ids" and "labels" as required by TransfoXL
            model.

        """
        batch = self._tensorize_batch(examples)
        return {"input_ids": batch, "labels": batch}

    def _tensorize_batch(self, examples: List[torch.Tensor]) -> torch.Tensor:
        """Collate a list of tensors

        Args:
            examples: List of tensors. Tensors must be identical in all but the
                first dimension.

        Returns:
            Batch made of the input tensors. All tensors are padded to the same
            length and then stacked along new dimension.

        Raises:
            ValueError: If tokensizer doesn't expose padding token.

        """
        max_length = max([example.size(0) for example in examples])
        are_tensors_same_length = all(example.size(0) == max_length for example in examples)
        if are_tensors_same_length:
            return torch.stack(examples, dim=0)

        if self.tokenizer.pad_token is None:
            raise ValueError(
                "You are attempting to pad samples but the tokenizer you are using"
                f" ({self.tokenizer.__class__.__name__}) does not have one."
            )
        return torch.stack([
                    Collater._pad(
                        example,
                        max_length,
                        padding_value=self.tokenizer.pad_token_id
                    ) for example in examples
                ],
                dim=0
            )

    @staticmethod
    def _pad(tensor: torch.Tensor, length: int, padding_value: int) -> torch.Tensor:
        """Pad tensor to specified length

        Args:
            tensor: Tensor to be padded.
            length: Length to which to pad.
            padding_value: Value used for padding.

        Returns:
            Padded version of the same tensor

        """
        return F.pad(tensor, pad=(length-tensor.size(0), 0), mode='constant', value=padding_value)

    def collate_batch(self, examples: List[torch.Tensor]) -> Dict[str, torch.Tensor]:
        """Collate a list of tensors

        Forwards to __call__ . Serves to satisfy older interfaces.

        Args:
            examples: List of tensors. Tensors must be identical in all but the
                first dimension.

        Returns:
            Batch made of the input tensors. All tensors are padded to the same
            length and then stacked along new dimension.

            Returns same data as "input_ids" and "labels" as required by TransfoXL
            model.

        """
        return self.__call__(examples)
