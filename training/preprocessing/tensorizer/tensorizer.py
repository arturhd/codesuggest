from typing import List, Tuple
import torch
import torch.nn.functional as F
from tokenizer.tokenizer import WordPieceTokenizer


class Tensorizer:
    """The Tensorizer encodes files into chunks of token ids.
    An Index marks chunks from the same file.

    Both the chunks of token ids and the index are saved as tensors.

    The index looks like [0,0,0, ..., 101, 101, ...] where index[i] is the
    file index for sample i. Samples from the same file will have the same
    file index.
    """

    def __init__(self, filepath_to_vocab: str, chunksize: int = 128):
        """Initializes class from vocab.

        Args:
            filepath_to_vocab: Filepath to vocab file that defines the padding token.
            chunksize: Number on tokens in a chunk of encoded data. (Default: 128)

        """
        self._tokenizer = WordPieceTokenizer(
            vocab_file=filepath_to_vocab
            )
        self._chunksize = chunksize

    def __call__(
                self,
                paths_to_datafiles: List[str]
            ) -> Tuple[torch.LongTensor, torch.LongTensor]:
        """Encode and chunk files

        Forwards to make_tensorized_index_and_data

        Args:
            paths_to_datafiles: List of filepaths to files that are to be encoded.

        Returns:
            Index and data tensor.

        """
        return self.make_tensorized_index_and_data(paths_to_datafiles)

    def make_tensorized_index_and_data(
                self,
                paths_to_datafiles: List[str]
            ) -> Tuple[torch.LongTensor, torch.LongTensor]:
        """Encode and chunk files

        Args:
            paths_to_datafiles: List of filepaths to files that are to be encoded.

        Returns:
            Index and data tensor.

        """
        samples = []
        index = torch.LongTensor([])
        count = 0
        total = len(paths_to_datafiles)
        for filepath in paths_to_datafiles:
            encoding = self._encode(filepath)
            chunks = torch.split(torch.tensor(encoding.ids, dtype=torch.long), self._chunksize)
            chunks = list(chunks)
            chunks[-1] = Tensorizer._pad(
                chunks[-1],
                length=self._chunksize,
                padding_value=self._tokenizer.pad_token_id
                )
            samples += chunks
            index = torch.cat((index, torch.LongTensor([count]*len(chunks))))
            count += 1
            if count % 1000 == 0:
                print("Loading Data: {0}".format(count/total))
        return (index, torch.stack(samples, dim=0))

    def _encode(self, filepath: str):
        """Encode single file

        Args:
            filepath: Filepath to file that is to be encoded.

        Returns:
            Encoding of the entire file.

        """
        try:
            with open(filepath, encoding="utf8") as file:
                return self._tokenizer.encode(Tensorizer._add_eol_tokens(file.read()))
        except (OSError, IOError) as err:
            print('Couldn\'t read file: ' + filepath)
            print("Error: {0}".format(err))
            return self._tokenizer.encode('')

    @staticmethod
    def _add_eol_tokens(input_str):
        """Replace all \n with [EOL]

        Args:
            input_str: Input string.

        Returns:
            Input string with [EOL] instead of \n.

        """
        return input_str.replace('\n', '[EOL]')

    @staticmethod
    def _pad(tensor: torch.Tensor, length: int, padding_value: int) -> torch.Tensor:
        """Pad tensor to specified length

        Args:
            tensor: Tensor to be padded.
            length: Length to which to pad.
            padding_value: Value used for padding.

        Returns:
            Padded version of the same tensor

        """
        return F.pad(tensor, pad=(length-tensor.size(0), 0), mode='constant', value=padding_value)
