from typing import List
import torch
from torch.utils.data import Dataset
from tokenizer.tokenizer import WordPieceTokenizer
from util.fileoperations import load_paths


class RandomAccessDataset(Dataset):
    """This dataset generates data samples from source files.

    Preloads Data.

    """

    def __init__(
                self,
                filepath_to_indexfile: str,
                filepath_to_vocab: str,
                path_to_dataset: str
            ):
        """Initializes dataset from file paths.

        Args:
            path_to_index_file: Filepath to file that contains one filepath per line.
            filepath_to_vocab: Filepath to the vocabulary file to be used to encode
                the data.
            path_to_dataset: Filepath to a directory containing the dataset.
                Must be root to all filepaths in the index files.

        Attributes:
            paths_to_datafiles: List of filepaths to all source files accessable
                through this dataset.
            samples: List of all the samples generated from those source files.

        """
        self._tokenizer = WordPieceTokenizer(vocab_file=filepath_to_vocab)
        self.paths_to_datafiles = load_paths(path_to_dataset, [filepath_to_indexfile])
        self.samples = self.make_samples(chunksize=128)

    def make_samples(self, chunksize: int) -> List[torch.Tensor]:
        """Make list of samples.

        Prints Progress.

        Args:
            chunksize: Length of one sample.

        Returns:
            List of the samples generated from the source files.

        """
        samples = []
        count = 0
        total = len(self.paths_to_datafiles)
        for filepath in self.paths_to_datafiles:
            encoding = self.encode(filepath)
            chunks = torch.split(torch.tensor(encoding.ids, dtype=torch.long), chunksize)
            samples += chunks
            count += 1
            if count % 1000 == 0:
                print("Loading Data: {0}".format(count/total))
        return samples

    @staticmethod
    def add_eol_tokens(input_str: str) -> str:
        """Replace all \n with [EOL]

        Args:
            input_str: Input string.

        Returns:
            Input string with [EOL] instead of \n.

        """
        return input_str.replace('\n', '[EOL]')

    def encode(self, filepath: str):
        """Encode file to token ids

        Args:
            filepath: Filepath to file that is to be encoded.

        Returns:
            Encoding of the given file.

        Raises:
            OSError, IOError: If opening the file wasn't successful.

        """
        try:
            with open(filepath, encoding="utf8") as file:
                return self._tokenizer.encode(
                        RandomAccessDataset.add_eol_tokens(file.read())
                    )
        except (OSError, IOError) as err:
            print('Couldn\'t read file: ' + filepath)
            print("Error: {0}".format(err))
            return self._tokenizer.encode('')

    def __len__(self) -> int:
        """Return number of samples in this dataset.

        Returns:
            Number of samples in this dataset.
        """
        return len(self.samples)

    def __getitem__(self, index: int) -> torch.Tensor:
        """Return sample at index

        Args:
            index: Index of sample.

        Returns:
            Sample at the given index.

        """
        return self.samples[index]
