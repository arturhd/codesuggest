import torch
from torch.utils.data import IterableDataset


# IterableDatasets don't need to override __getitem__
# pylint: disable=abstract-method
class GroupedIterableDataset(IterableDataset):
    """This IterableDataset iterates over sequences of data.

    This dataset returns a sequence of samples. A sequence is defined
    as samples with the same index. The index is defined in a separate file.

    """

    def __init__(
                self,
                filepath_to_tensorized_index: str,
                filepath_to_tensorized_data: str
            ):
        """Initializes dataset from pre-tensorized data

        The index looks like [0,0,0, ..., 101, 101, ...] where index[i] is the
        file index for sample i. Samples from the same file will have the same
        file index.

        Args:
            filepath_to_tensorized_index: Filepath to the saved tensor of index.
            filepath_to_tensorized_data: Filepath to the saved tensor of data.

        """
        self._index = torch.load(filepath_to_tensorized_index)
        self._data = torch.load(filepath_to_tensorized_data)
        self._head = 0

    def __iter__(self) -> IterableDataset:
        """Initiate new iteration of this object

        Returns:
            This, ready to iterate from beginning.
        """
        self._head = 0
        return self

    def __next__(self) -> torch.Tensor:
        """Return next sequence of samples

        Returns:
            Next sequence of samples.
        """
        if self._head >= len(self._index):
            raise StopIteration
        begin = self._head
        end = begin
        fileindex = self._index[begin]
        while(end < len(self._index) and self._index[end] == fileindex):
            end += 1
        self._head = end
        return self._data[begin:end]

    def __len__(self) -> int:
        """Return number of samples in the data.

        Attention: This is NOT the number of sequences, i.e. number of items
        in this iterator.

        Justification:
            A. The number of sequences can't be calculated without a forwardpass
                over the index. This is too expensive for __len__ .
            B. BatchedIterableDataset needs the number of samples, not the number
                of sequences.

        Returns:
            Number of samples in the data.
        """
        return len(self._index)
